# README

**Website:** https://euroteplo.com.ua

**Playwright installation**
`npm i -D @playwright/test`

**Install supported browsers**
`npx playwright install`

**To run tests:**
`npx playwright test`

**Config:**
- Browser: Chromium;
- Viewport: width: 1366, height: 768;
- Headless: True;
- Screenshot: Only-on-failure (Will be added to 'test-results' folder);
