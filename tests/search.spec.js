const { test, expect } = require('@playwright/test');


test('search and sorting', async ({ baseURL, page }) => {
    await page.goto(baseURL);
    await page.fill('[placeholder="Введите название товара или артикул"]', '2131231231');
    await page.click('.button_search');
    await expect(page).toHaveURL('https://euroteplo.com.ua/products?keyword=2131231231');
    await page.click('text=По вашему запросу ничего не найдено');
    await page.fill('[placeholder="Введите название товара или артикул"]', '!@#$%^&*()-_=+s');
    await page.click('.button_search');
    await expect(page).toHaveURL('https://euroteplo.com.ua/products?keyword=%21%40%23%24%25%5E%26*%28%29-_%3D%2Bs');
    await page.click('text=По вашему запросу ничего не найдено');

    await page.fill('[placeholder="Введите название товара или артикул"]', '   ');
    await page.click('.button_search');
    await expect(page).toHaveURL('https://euroteplo.com.ua/products?keyword=+++');

    await page.selectOption('select', '/products?keyword=+++&sort=buyable');
    await expect(page).toHaveURL('https://euroteplo.com.ua/products?keyword=+++&sort=buyable'); 
    //await page.screenshot({path: 'screenshots/sort-buyable.png', fullPage: true});
    await page.selectOption('select', '/products?keyword=+++&sort=price');
    await expect(page).toHaveURL('https://euroteplo.com.ua/products?keyword=+++&sort=price');
    //await page.screenshot({path: 'screenshots/sort-price.png', fullPage: true});
    await page.selectOption('select', '/products?keyword=+++&sort=name');
    await expect(page).toHaveURL('https://euroteplo.com.ua/products?keyword=+++&sort=name');
     //await page.screenshot({path: 'screenshots/sort-name.png', fullPage: true});
    await page.selectOption('select', '/products?keyword=+++&sort=position');
    await expect(page).toHaveURL('https://euroteplo.com.ua/products?keyword=+++&sort=position');
    //await page.screenshot({path: 'screenshots/sort-position.png', fullPage: true});
  });
