const { test, expect } = require('@playwright/test');

test('removing items from cart', async ({ baseURL, page }) => {
    await page.goto(baseURL);
    await page.click('text=4 233 грн УЗНАТЬ ЦЕНУ КУПИТЬ >> input[type="submit"]');
    await page.click('text=Товар добавлен в корзину 4 233 грн Итого: 4 233 грн Вернуться в каталог Оформить >> img[alt="Удалить из корзины"]');
    await expect(page).toHaveURL('https://euroteplo.com.ua/cart/');
    const emptycart = await page.innerText('.products_h1');
    expect(emptycart).toBe('Корзина пуста');
      //Checking errors in console
    page.on('console', msg => {
    if (msg.type() === 'error'){
      console.log(`TEST: removing items from cart. Error text: "${msg.text()}"`);
    }});
    await page.click('img[alt="Магазин отопительной техники Евротепло"]');
    await expect(page).toHaveURL('https://euroteplo.com.ua/');
    await page.click('text=4 233 грн УЗНАТЬ ЦЕНУ КУПИТЬ >> input[type="submit"]');
    await page.click('text=Оформить');
    await expect(page).toHaveURL('https://euroteplo.com.ua/cart/');
    await page.click('text=Комплект инсталляции Cersanit Link + клавиша Link хром + унитаз Cersanit Delfi 4 >> img[alt="Удалить из корзины"]');
    await expect(page).toHaveURL('https://euroteplo.com.ua/cart/');
    expect(emptycart).toBe('Корзина пуста');
    //Checking errors in console
    page.on('console', msg => {
    if (msg.type() === 'error'){
      console.log(`TEST: removing items from cart. Error text: "${msg.text()}"`);
    }});
  });

  test('form in cart', async ({ baseURL, page }) => {
    await page.goto(baseURL);
    await page.click('text=335 грн УЗНАТЬ ЦЕНУ КУПИТЬ >> input[type="submit"]');
    await page.click('text=Оформить');
    await expect(page).toHaveURL('https://euroteplo.com.ua/cart/');
    //Submit form with empty values
    await page.click('text=ОФОРМИТЬ ЗАКАЗ');
    await expect(page).toHaveURL('https://euroteplo.com.ua/cart/');
    //Name validation
    await page.fill('input[name="name"]', '    ');
    //wait page.click('text=ОФОРМИТЬ ЗАКАЗ');
    await page.fill('input[name="name"]', '~!@#$%^&*()=');
    //await page.click('text=ОФОРМИТЬ ЗАКАЗ');
    await page.fill('input[name="name"]', ' 123');
    //await page.click('text=ОФОРМИТЬ ЗАКАЗ');
    //Email validation
    await page.fill('input[name="email"]', '    ');
    //await page.click('text=ОФОРМИТЬ ЗАКАЗ');
    await page.fill('input[name="email"]', 'asda.com');
    //await page.click('text=ОФОРМИТЬ ЗАКАЗ');
    await page.fill('input[name="email"]', 'adadA@');
    //await page.click('text=ОФОРМИТЬ ЗАКАЗ');
    await page.fill('input[name="email"]', '@aasd.aca');
    //await page.click('text=ОФОРМИТЬ ЗАКАЗ');
    await page.fill('input[name="email"]', 'asdas@.asda');
    //await page.click('text=ОФОРМИТЬ ЗАКАЗ');
    await page.fill('input[name="email"]', 'asdaa@adsa');
    //await page.click('text=ОФОРМИТЬ ЗАКАЗ');
    await page.fill('input[name="email"]', '~!@#$%^&*()=');
    //await page.click('text=ОФОРМИТЬ ЗАКАЗ');
    //Phone validation
    await page.fill('input[name="phone"]', '123456789');
    //await page.click('text=ОФОРМИТЬ ЗАКАЗ');
    //Address validation
    //await page.fill('input[name="address"]', 'new ');
    //await page.click('text=New YorkНью-Йорк, США >> span');
    //await page.click('text=ОФОРМИТЬ ЗАКАЗ');
    //Comments validation
    await page.fill('textarea[name="comment"]', '312312312312');
    await page.click('text=ОФОРМИТЬ ЗАКАЗ');
    //Screenshot
    //await page.screenshot({path: 'screenshots/cart-form.png', fullPage: true});

  });
