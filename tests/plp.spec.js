const { test, expect } = require('@playwright/test');

test('filters and adding to cart on PLP', async ({ baseURL, page }) => {
    await page.goto(baseURL); 
    await page.click('text=Сантехника');
    await expect(page).toHaveURL('https://euroteplo.com.ua/catalog/santehnika/');
    await page.click('.menutop-title[category_id="355"]');
    await expect(page).toHaveURL('https://euroteplo.com.ua/catalog/dushevye-kabiny-i-aksessuary/');
    await page.click('img[alt="Душевые кабины"]');
    await expect(page).toHaveURL('https://euroteplo.com.ua/catalog/dushevye-kabiny_2/');
    await page.click('text=Eger');
    await expect(page).toHaveURL('https://euroteplo.com.ua/catalog/dushevye-kabiny_2/?141%5B%5D=Eger');
    await page.click('text=квадратная');
    await expect(page).toHaveURL('https://euroteplo.com.ua/catalog/dushevye-kabiny_2/?141%5B%5D=Eger&853%5B%5D=%D0%BA%D0%B2%D0%B0%D0%B4%D1%80%D0%B0%D1%82%D0%BD%D0%B0%D1%8F');
  
    //Screenshot
    //await page.screenshot({path: 'screenshots/PLPfilter.png', fullPage: true});
    
    await page.click('text=10 754 грн УЗНАТЬ ЦЕНУ КУПИТЬ >> input[type="submit"]');
    await page.click('#add_tables_content >> text=Оформить');
    await expect(page).toHaveURL('https://euroteplo.com.ua/cart/');
    const totalpriceincart = await page.innerText('.totalprice >> strong');
    expect(totalpriceincart).toBe('10 754');
      //Checking errors in console
      page.on('console', msg => {
        if (msg.type() === 'error'){
          console.log(`TEST: filters and adding to cart on PLP. Error text: "${msg.text()}"`);
        }});
    //Screenshot
    //await page.screenshot({path: 'screenshots/PLPfilter2.png', fullPage: true});
  });

  test('breadcrumbs', async ({ baseURL,page }) => {
    await page.goto('/catalog/knopki-dlya-smyva/alcaplast/');
    
    await page.click('#path >> text=Кнопки для смыва');
    await expect(page).toHaveURL('https://euroteplo.com.ua/catalog/knopki-dlya-smyva');
    await page.click('#path >> text=Инсталяции');
    await expect(page).toHaveURL('https://euroteplo.com.ua/catalog/instalyatsii_2');
    await page.click('#path >> text=Главная');
    await expect(page).toHaveURL('https://euroteplo.com.ua/');

    await page.goto('/catalog/dushevye-kabiny_2/');
    await page.click('#path >> text=Душевые кабины и аксессуары');
    await expect(page).toHaveURL('https://euroteplo.com.ua/catalog/dushevye-kabiny-i-aksessuary');
    await page.click('#path >> text=Сантехника');
    await expect(page).toHaveURL('https://euroteplo.com.ua/catalog/santehnika');
    await page.click('#path >> text=Главная');
    await expect(page).toHaveURL('https://euroteplo.com.ua/');

    await page.goto('/catalog/multisplit-sistemy/');
    await page.click('#path >> text=Кондиционеры');
    await expect(page).toHaveURL('https://euroteplo.com.ua/catalog/konditsionery');
    await page.click('#path >> text=Климат');
    await expect(page).toHaveURL('https://euroteplo.com.ua/catalog/klimat');
    await page.click('#path >> text=Главная');
    await expect(page).toHaveURL('https://euroteplo.com.ua/');

    await page.goto('/contact');
    await page.click('#path >> text=Главная');
    await expect(page).toHaveURL('https://euroteplo.com.ua/');
});

test('sorting on PLP', async ({ baseURL, page }) => {
  await page.goto(baseURL);
  await page.click('img[alt="Mirado 500/96 BM"]');
  await expect(page).toHaveURL('https://euroteplo.com.ua/products/mirado-50096-bm3011035');
  await page.click('text=Mirado');
  await expect(page).toHaveURL('https://euroteplo.com.ua/catalog/alyuminievye-i-bimetallicheskie-radiatory_2/mirado/');
  await page.selectOption('select', '/catalog/alyuminievye-i-bimetallicheskie-radiatory_2/mirado/?sort=buyable');
  await expect(page).toHaveURL('https://euroteplo.com.ua/catalog/alyuminievye-i-bimetallicheskie-radiatory_2/mirado/?sort=buyable');
  await page.selectOption('select', '/catalog/alyuminievye-i-bimetallicheskie-radiatory_2/mirado/?sort=price');
  await expect(page).toHaveURL('https://euroteplo.com.ua/catalog/alyuminievye-i-bimetallicheskie-radiatory_2/mirado/?sort=price');
  await page.selectOption('select', '/catalog/alyuminievye-i-bimetallicheskie-radiatory_2/mirado/?sort=name');
  await expect(page).toHaveURL('https://euroteplo.com.ua/catalog/alyuminievye-i-bimetallicheskie-radiatory_2/mirado/?sort=name');
  await page.selectOption('select', '/catalog/alyuminievye-i-bimetallicheskie-radiatory_2/mirado/?sort=position');
  await expect(page).toHaveURL('https://euroteplo.com.ua/catalog/alyuminievye-i-bimetallicheskie-radiatory_2/mirado/?sort=position');
});
