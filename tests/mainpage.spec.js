const { test, expect } = require('@playwright/test');

test('header elements visibility', async ({ baseURL, page }) => {
  await page.goto(baseURL); 
  await expect(page).toHaveTitle("Интернет-магазин EuroTeplo™: Котлы(газовые, электрические, твердотопливные), Водонагреватели(бойлера, колонки,аккумулирующие баки, косвенники), Радиаторы, Насосы, Трубы и фитинги, Баки, Теплый пол, Отопительное оборудование");
  //Checking errors in console
  page.on('console', msg => {
      if (msg.type() === 'error'){
        console.log(`Test: header elements visibility. Error text: "${msg.text()}"`);
      }});
  //Checking empty shopping cart
  const emptycart = await page.innerText('#cart_informer.header_top');
  expect(emptycart).toBe('Корзина пуста');
  //Checking phone numbers visibility
  const numbers = await page.innerText('.phone_top');
  expect(numbers).toBe('(096) 682-21-75 (093) 608-21-89');
  //Screenshot
  //await page.screenshot({path: 'screenshots/header.png', fullPage: true});
});


test('Logo redirect test1', async ({ baseURL, page }) => {

await page.goto(baseURL); 
await page.click('text=Контакты');
expect(page.url()).toBe('https://euroteplo.com.ua/contact');

await page.click('img[alt="Магазин отопительной техники Евротепло"]');
expect(page.url()).toBe('https://euroteplo.com.ua/');

await page.click('text=Доставка');
expect(page.url()).toBe('https://euroteplo.com.ua/dostavka');

await page.click('img[alt="Магазин отопительной техники Евротепло"]');
expect(page.url()).toBe('https://euroteplo.com.ua/');

await page.click('text=Гарантия');
expect(page.url()).toBe('https://euroteplo.com.ua/garantiya');

await page.click('img[alt="Магазин отопительной техники Евротепло"]');
expect(page.url()).toBe('https://euroteplo.com.ua/');

await page.click('text=Новости');
expect(page.url()).toBe('https://euroteplo.com.ua/blog');

await page.click('img[alt="Магазин отопительной техники Евротепло"]');
expect(page.url()).toBe('https://euroteplo.com.ua/');
});

test('Adding item to cart', async ({ baseURL, page }) => {

  await page.goto(baseURL); 
  const slidervisibility = await page.isVisible('.slick-track');
  expect(slidervisibility).toBeTruthy();
  const itemname = await page.innerText('#name_on_5568');
  expect(itemname).toBe('Комплект инсталляции Cersanit Link + клавиша Link хром + унитаз Cersanit Delfi');
  const itemprice = await page.innerText('#prise_on_5568');
  expect(itemprice).toBe('4 233');

  await page.click('text=4 233 грн УЗНАТЬ ЦЕНУ КУПИТЬ >> input[type="submit"]');

  await page.click('text=Оформить');
  await expect(page).toHaveURL('https://euroteplo.com.ua/cart/');

  const totalpriceincart = await page.innerText('.totalprice >> strong');
  expect(totalpriceincart).toBe('4 233');
    //Checking errors in console
    page.on('console', msg => {
      if (msg.type() === 'error'){
        console.log(`TEST: Adding item to cart. Error text: "${msg.text()}"`);
      }});
  //Screenshot
  //await page.screenshot({path: 'screenshots/cart.png', fullPage: true});
  });
