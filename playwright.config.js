// playwright.config.js
// @ts-check

/** @type {import('@playwright/test').PlaywrightTestConfig} */
const config = {
    use: {
      baseURL: 'https://euroteplo.com.ua/',
      headless: true,
      screenshot:'only-on-failure',
    },
    projects: [
      {
        name: 'Chromium',
        use: {
          browserName: 'chromium',
          viewport: { width: 1366, height: 768 },        },
      },
      /* {
        name: 'Firefox',
        use: { browserName: 'firefox',
        viewport: { width: 1366, height: 768 },
      },
        
      },
      {
        name: 'WebKit',
        use: { browserName: 'webkit',
        viewport: { width: 1366, height: 768 },
      },
      }, */
    ],
  };
  
  module.exports = config;